import m from "mithril";
import  tableControllers  from "./views/table";

let rowPerPage = 10;  /// max value can be 1000
let totalRows = 100;
let startingRow = 1;
let currnetDataSource;

const generateDataSource = (totalRows) => {
  ///////  creating 100000 entries and storing it in a table
  const headers = ["S No.", "Locn_Nbr", "Online_Ord_Id", "KSN_Id", "SKU_Prc_Type_Cd"]
  const dataSource = [];
  totalRows = totalRows < 100000 ? totalRows : 100000;
  for (let i = 0; i < totalRows; i++) {
    dataSource[i] = [];
    headers.forEach(header => {
      if (header == "S No.") {
        dataSource[i].push(i + 1);
      } else {
        dataSource[i].push(parseInt(Math.random() * 100));
      }
    });
  }

  return dataSource;
}


const changeTable = (value,event) => {  
  value = Number(value);  
  switch (event) {
    case "change-row-perpage":
      if (value > 0 && value <= 1000 && value <= totalRows){
        rowPerPage = value;
      }      
      break;
    case "change-total-rows":
      if(value > 0 && value <= 1000000){
        totalRows = value;
        currnetDataSource = generateDataSource(totalRows);
      }
      break;
    case "change-starting-row":      
    if(value > 0 && value <= totalRows - rowPerPage){
      startingRow = value;
    }
      break;
  }
  m.render(target,
    m(tableControllers,
      {
        onchange: changeTable,
        onbuttonclick: changeView,
        rowPerPage: rowPerPage,
        totalRows: totalRows,
        startingRow: startingRow,
        dataSource: currnetDataSource.slice(startingRow - 1, startingRow - 1+rowPerPage)
      })
  )
}


const changeView = (value) => {  
  switch (value) {
    case "rowup":
      startingRow = startingRow > 1 ? startingRow - 1 : startingRow;
      break;
    case "pageup":
     //  if it is the first page then set starting value 1 , else shift page
     if(startingRow < rowPerPage){
       startingRow = 1;
     }else{
       startingRow-=rowPerPage;
     }
      break;
    case "firstpage":
      startingRow = 1;
    break;
    case "rowdown":
      if (startingRow <= totalRows - rowPerPage) {
        startingRow ++;
      } 
      break;
    case "pagedown":
      startingRow+=rowPerPage;
      if (startingRow > totalRows - rowPerPage) {
        startingRow = totalRows - (rowPerPage) + 1;
      }

      break;
    case "lastpage":
      startingRow = totalRows - (rowPerPage)+1 ;
      break;
  }
  m.render(target,
    m(tableControllers,
      {
        onchange: changeTable,
        onbuttonclick: changeView,
        rowPerPage: rowPerPage,
        totalRows: totalRows,
        startingRow: startingRow,
        dataSource: currnetDataSource.slice(startingRow - 1, startingRow - 1 + rowPerPage)
      })
  )
}


const target = document.getElementById("root");
currnetDataSource = generateDataSource(totalRows);
m.render(target,
   m(tableControllers, 
      { onchange: changeTable,
        onbuttonclick: changeView,
        rowPerPage: rowPerPage,
        totalRows: totalRows,
        startingRow: startingRow,
        dataSource : currnetDataSource.slice(startingRow-1,rowPerPage)
      })
  )


