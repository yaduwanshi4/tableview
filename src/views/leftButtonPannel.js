import m from "mithril";

const leftButtonPannel = {
  view: (vnode) => {
    return (
      m(".left-pannel",
        m("button.f6 link dim br3 ph3 pv2 mb2 dib white bg-hot-pink", { onclick: vnode.attrs.func, name:"rowup"},"|<"),
        m("button.f6 link dim br3 ph3 pv2 mb2 dib white bg-hot-pink", { onclick: vnode.attrs.func, name:"pageup"}, "<<"),
        m("button.f6 link dim br3 ph3 pv2 mb2 dib white bg-hot-pink", { onclick: vnode.attrs.func, name:"firstpage"}, "<"))
    );
  }
};

export default leftButtonPannel;
