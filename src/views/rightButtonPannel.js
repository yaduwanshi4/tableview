import m from "mithril";

const rightButtonPannel = {
  view: (vnode) => {
    return (
      m(".right-pannel",
        m("button.f6 link dim br3 ph3 pv2 mb2 dib white bg-hot-pink", { onclick: vnode.attrs.func,name: "lastpage"}, ">"),
        m("button.f6 link dim br3 ph3 pv2 mb2 dib white bg-hot-pink", { onclick: vnode.attrs.func,name: "pagedown"}, ">>"),
        m("button.f6 link dim br3 ph3 pv2 mb2 dib white bg-hot-pink", { onclick: vnode.attrs.func,name: "rowdown"}, ">|")        
      )
    );
  }
};

export default rightButtonPannel;
