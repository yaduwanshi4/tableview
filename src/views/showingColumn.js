import m from "mithril";
///  input box containg the values of how many rows we are showing on 1 page

const showingColumn = {
  view: (vnode) => {
    return m(".showing",
      m("label", "showing"),
      m("input.show-button",
        {
          oninput: m.withAttr("value",
            function (value) {
              vnode.attrs.func(value);
            }
          ),
          value: vnode.attrs.value
        }
      ))
  }
};

export default showingColumn;

