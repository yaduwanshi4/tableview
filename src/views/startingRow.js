import m from "mithril";
///  input box containg the values of how many rows we are showing on 1 page

const startingRow = {
  view: (vnode) => {
    return m(".starting",
      m("label", "starting at row"),
      m("input.start-button", 
        {
          oninput: m.withAttr("value",
            function (value) {                
              vnode.attrs.func(value);
            }
          ),
          value:vnode.attrs.value
        }
    ))
  }
};

export default startingRow;