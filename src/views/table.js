import m from "mithril";
import leftButtonPannel from "./leftButtonPannel";
import rightButtonPannel from "./rightButtonPannel";
import showingColumn from "./showingColumn";
import totalRows from "./totalRows";
import startingRow from "./startingRow";


const Headers = ["S No.","Locn_Nbr","Online_Ord_Id","KSN_Id","SKU_Prc_Type_Cd"]

const tableControllers = {
  view: (vnode) => {      
    return m(".main-table",
      m(".w-100 pa3 ph5-ns bg-white",
        m(leftButtonPannel,
          {
            func: (event) => {
              vnode.attrs.onbuttonclick(event.target.name);
              event.preventDefault();
            }
          }
        ),
        m(showingColumn, {
          func: (value) => {
            vnode.attrs.onchange(value, "change-row-perpage")
          },
          value: vnode.attrs.rowPerPage
        }),
        m(totalRows, {
          func: (value) => {
            vnode.attrs.onchange(value, "change-total-rows")
          },
          value: vnode.attrs.totalRows
        }),
        m(startingRow, {
          func: (value) => {
            vnode.attrs.onchange(value, "change-starting-row")
          },
          value: vnode.attrs.startingRow
        }),
        m(rightButtonPannel,
          {
            func: (event) => {
              vnode.attrs.onbuttonclick(event.target.name);
              event.preventDefault();
            }
          }
        )),
      m('table.controller', m('tr.head-row', Headers.map( value => m("th",value))),
        vnode.attrs.dataSource.map( row => m("tr",
          row.map(value => m("td",value))
        ))
      
    ));
  }
};



export default tableControllers;