import m from "mithril";
///  input box containg the values of how many rows we are showing on 1 page

const totalRows = {
  view: (vnode) => {
    return m(".total",
      m("label", "rows out of"),
      m("input.total-button",
        {
          oninput: m.withAttr("value",
            function (value) {
              vnode.attrs.func(value);
            }
          ),
          value: vnode.attrs.value
        }
      ))
  }
};

export default totalRows;
